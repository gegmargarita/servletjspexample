package com.egs;


import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * Created by gegma on 5/2/2019.
 */
public class HelloWorld extends HttpServlet {

@Override
    protected  void doGet(HttpServletRequest request , HttpServletResponse response)
                        throws ServletException, IOException {

    RequestDispatcher rd= request.getRequestDispatcher("index.jsp");
    rd.forward(request , response);



}
}